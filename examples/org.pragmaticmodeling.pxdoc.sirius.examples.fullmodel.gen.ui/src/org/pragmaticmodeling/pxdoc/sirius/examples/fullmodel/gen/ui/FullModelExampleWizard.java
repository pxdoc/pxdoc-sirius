package org.pragmaticmodeling.pxdoc.sirius.examples.fullmodel.gen.ui;

/**
 * Wizard class
 */
public class FullModelExampleWizard extends AbstractFullModelExampleWizard {
	
	/**
	 * Add the custom page to the generation launching wizard
	 */
	@Override
	public void addPages() {
		super.addPages();
		addPage(new FullModelGenerationOptionsPage("Custom Options"));
	}

}
	 
	
