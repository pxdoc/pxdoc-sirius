package org.pragmaticmodeling.pxdoc.sirius.examples.fullmodel.gen.ui;

import org.osgi.framework.Bundle;

import com.google.inject.Injector;

import fr.pragmaticmodeling.pxdoc.runtime.eclipse.guice.AbstractGuiceAwareExecutableExtensionFactory;

public class FullModelExampleExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	   protected Bundle getBundle() {
	       return FullModelExampleActivator.getInstance().getBundle();
	   }
	    
	   @Override
	   protected Injector getInjector() {
	       return FullModelExampleActivator.getInstance().getInjector();
	   }
	   
}
	
