package org.pragmaticmodeling.pxdoc.sirius.examples.fullmodel.gen.ui;

import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.commands.DefaultPropertyTester;
import org.eclipse.emf.ecore.EObject;

/**
 * PxDocModel property tester and model provider
 */
public class AbstractFullModelExamplePropertyTester extends DefaultPropertyTester {

	public AbstractFullModelExamplePropertyTester() {
		super(EObject.class);
	}
}

