package org.pragmaticmodeling.pxdoc.sirius.examples.fullmodel.gen.ui;

import org.eclipse.emf.ecore.EObject;
import org.pragmaticmodeling.pxdoc.sirius.examples.fullmodel.gen.ui.AbstractFullModelExamplePropertyTester;

public class FullModelExamplePropertyTester extends AbstractFullModelExamplePropertyTester {

	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
		if (receiver instanceof EObject) {
			return true;
		}
		return super.test(receiver, property, args, expectedValue);
	}
}

