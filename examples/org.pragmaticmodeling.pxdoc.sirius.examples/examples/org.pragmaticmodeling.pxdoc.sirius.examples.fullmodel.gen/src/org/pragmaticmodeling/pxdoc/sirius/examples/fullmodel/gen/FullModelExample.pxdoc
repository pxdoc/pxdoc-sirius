package org.pragmaticmodeling.pxdoc.sirius.examples.fullmodel.gen

import org.eclipse.emf.ecore.EObject
import org.pragmaticmodeling.pxdoc.common.lib.CommonServices
import org.pragmaticmodeling.pxdoc.common.lib.DefaultDescriptionProvider
import org.pragmaticmodeling.pxdoc.diagrams.lib.DiagramServices
import org.pragmaticmodeling.pxdoc.emf.lib.DocKind
import org.pragmaticmodeling.pxdoc.emf.lib.EmfServices
import org.pragmaticmodeling.pxdoc.runtime.sirius.SiriusDiagramsProvider

/**
 * Generic pxDoc generator.
 */
pxDocGenerator FullModelExample with:CommonServices, EmfServices, DiagramServices styles: Title, Normal
{

	/**
	 * Entry point. 
	 */
	root template main(EObject model) {

		/** Defines variables set in the tailored launching wizard: option page added to:
		 *   - Define a title, a company name and a document version displayed on the first page of the document
		 *   - Choose a 'strategy': display attributes in table or in 'bullet-point' style
		 *   - Choose to suggest improvements (or not) when an element description is missing
		 * 
		 * The custom wizard page is defined in EMFSampleOptionsPage.java (org.pragmaticmodeling.pxdoc.emf.gen.ui - src package)
		 * It is added to the wizard in EMFExampleWizard.java of the same package
		 */
		var specTitle = if (getProperty("title") === null)
				'Specification of ' + model.text
			else
				getProperty("title") as String
		var societyName = getProperty("society") as String
		var version = getProperty("version") as String
		var DocKind strategy = getProperty("generationStrategy") as DocKind
		var suggest = getProperty("suggestImprovements") as Boolean

		// Commons lib configuration : set a description provider able to get information about object description
		descriptionProvider = new DefaultDescriptionProvider(this)

		// Diagrams lib configuration : Capella Diagrams Provider injected in DiagramServices
		diagramProvider = new SiriusDiagramsProvider

		document properties:title=specTitle, Company=societyName variables: version=version {

			// compute a name for the header manual sub-document
			// var headerFile = launcher.filename.replaceAll(".docx", "-header.docx")
			// You can define the first page as a Manual Part, which will be inserted as a subdocument
			// Manual part are not modified when regenerating the doc, so you can manually 
			// add signatures, release notes,..., by updating directly the  file "header.docx"
			// created when you generate the doc for the first time.
			// You just need to uncomment the following line:			
			// subDocument headerFile fromTemplate: "templates/header.docx"
			// Describing the first page of the document (if not provided 
			// by the manual part defined just beyond)
			§ // paragraph break
			§
			§
			§
			§
			#Title { // apply style Title from the stylesheet
				'Full description of '
				newLine
				specTitle // Title will be specified in the Generation Wizard, and stored as a property of the document
			}
			§
			#Normal align:center {
				image path:'images/companyLogo.png'
				§
				italic {societyName } // Same as specTitle
				§
				§
				underline {'Version: ' version }
			}

			newPage // page break
			toc title:'Table of Content' titleStyle:Title // table of content 
			§
			newPage

			suggestImprovements = suggest
			// Apply 'description' template, defined below
			apply description(model, strategy, 0)

		}

	}

	/**
	 * Demonstrates how to mix/reuse various way of presenting elements.
	 */
	template description(EObject element, DocKind strategy, int level) {
		switch element {
//			MyConcept: {
//				apply myConceptTemplate(element)
//			}
			default: {
				// apply the 'genericDoc' template from the EmfServices module
				apply genericDoc(element, strategy, level, false)
			}
		}
		// Recursively apply this template to all sub-elements
		apply description(element.eContents, strategy, level + 1)
	}


}
              