package org.pragmaticmodeling.pxdoc.sirius.examples.fullmodel.gen;
		
import org.pragmaticmodeling.pxdoc.sirius.examples.fullmodel.gen.AbstractFullModelExampleModule;

import com.google.inject.Binder;
			
public class FullModelExampleModule extends AbstractFullModelExampleModule {
			
	@Override
	public void configure(Binder binder) {
		super.configure(binder);
	}

}