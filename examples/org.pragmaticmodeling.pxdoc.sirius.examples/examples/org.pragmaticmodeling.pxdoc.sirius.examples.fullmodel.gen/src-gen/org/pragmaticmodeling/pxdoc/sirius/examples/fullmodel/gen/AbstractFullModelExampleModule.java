package org.pragmaticmodeling.pxdoc.sirius.examples.fullmodel.gen;
				
import com.google.inject.Binder;
import com.google.inject.Module;
				
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
					
public abstract class AbstractFullModelExampleModule implements Module {
					
	@Override
	public void configure(Binder binder) {
		binder.bind(IPxDocGenerator.class).to(FullModelExample.class);
	}

}