package org.pragmaticmodeling.pxdoc.enablers.sirius;

import java.util.List;

import fr.pragmaticmodeling.pxdoc.dsl.ui.enablers.AbstractPlatformEnabler;

public class SiriusEnabler extends AbstractPlatformEnabler {

	public SiriusEnabler() {
		super();
	}

	@Override
	public String getJavaClass() {
		return "org.pragmaticmodeling.pxdoc.enablers.sirius.SiriusInput";
	}

	@Override
	public List<String> getDevelopmentTimesBundles() {
		List<String> result = super.getDevelopmentTimesBundles();
		result.add("org.pragmaticmodeling.pxdoc.enablers.sirius");
		return result;
	}

	@Override
	public String getModelObject() {
		return "org.eclipse.emf.ecore.EObject";
	}

	@Override
	public List<String> getPxdocImports(boolean withPxdocLibs) {
		List<String> result = super.getPxdocImports(withPxdocLibs);
		if (withPxdocLibs) {
			result.add("org.pragmaticmodeling.pxdoc.runtime.sirius.SiriusDiagramsProvider");
			result.add("org.pragmaticmodeling.pxdoc.common.lib.DefaultDescriptionProvider");
		}
		return result;
	}

	@Override
	public List<String> getRequiredBundles(boolean withPxdocLibs) {
		List<String> result = super.getRequiredBundles(withPxdocLibs);
		result.add("org.pragmaticmodeling.pxdoc.runtime.sirius");
		result.add("org.pragmaticmodeling.pxdoc.plugins.diagrams");
		result.add("org.pragmaticmodeling.pxdoc.plugins.html2pxdoc");
		if (withPxdocLibs) {
			result.add("org.pragmaticmodeling.pxdoc.diagrams.lib");
			result.add("org.pragmaticmodeling.pxdoc.images.lib");
			result.add("org.pragmaticmodeling.pxdoc.common.lib");
			result.add("org.pragmaticmodeling.pxdoc.emf.lib");
		}
		return result;
	}

	@Override
	public String getRootTemplateDeclarations() {
		String result = "// Commons lib configuration : set a description provider able to get information about object description\n";
		result += "descriptionProvider = new DefaultDescriptionProvider(this)\n";
		result += "// Diagrams lib configuration : Sirius Diagrams Provider injected in DiagramServices\n";
		result += "diagramProvider = new SiriusDiagramsProvider\n";
		return result;
	}

	@Override
	public List<String> getWithModules() {
		List<String> result = super.getWithModules();
		result.add("org.pragmaticmodeling.pxdoc.diagrams.lib.DiagramServices");
		result.add("org.pragmaticmodeling.pxdoc.common.lib.CommonServices");
		result.add("org.pragmaticmodeling.pxdoc.emf.lib.EmfServices");
		return result;
	}

}
