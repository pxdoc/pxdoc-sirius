package org.pragmaticmodeling.pxdoc.runtime.sirius;

import org.pragmaticmodeling.pxdoc.diagrams.IDiagramProvider;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramQueryBuilder;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramsQueryRunner;
import org.pragmaticmodeling.pxdoc.diagrams.impl.DiagramQueryBuilderImpl;

public class SiriusDiagramsProvider implements IDiagramProvider<IDiagramQueryBuilder> {

	private IDiagramsQueryRunner runner;
	
	public SiriusDiagramsProvider() {
		super();
		this.runner = new SiriusDiagramsQueryRunner();
	}

	@Override
	public IDiagramQueryBuilder createDiagramsQuery(Object namespace) {
		return createQuery(namespace);
	}

	@Override
	public IDiagramQueryBuilder createQuery(Object namespace) {
		return new DiagramQueryBuilderImpl(namespace, runner);
	}

}
